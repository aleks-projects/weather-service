from flask import Blueprint, request, render_template
import requests
from datetime import datetime
import json
from Support_Functions.Support_Functions import db_connect, convert_to_C, get_collection_data, get_city_key

TwelveHours = Blueprint("TwelveHours", __name__, template_folder="templates", static_folder="static")


@TwelveHours.route("/twelve_hours/")
def twelve_hours():
    city_name = request.args.get("city")
    if city_name:
        city_key = get_city_key(city_name)
        date = datetime.now()
        local_time = int(date.strftime("%H")) + 1
        date = str(date.date())
        try:
            with db_connect() as client:
                call = get_collection_data(client, "TwelveHoursForecast")
                forecast = None
                for i in call.find():
                    if city_key == i["Key"] and date == i["Date"] and local_time == int(i["Forecast"][0]["Time"][:2]):
                        forecast = i["Forecast"]
                        break
        except:
            print("Cannot connect with DataBase")
        if forecast is not None:
            return render_template("TwelveHoursForecast.html", posts=forecast)
        params_2 = {"locationKey": city_key, "apikey": "d3XkGe1PEMS330oLGOw43MJf0J2deNdc"}
        response = requests.get('http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/{locationKey}',
                                params=params_2)
        response = json.loads(response.text)
        data = {"LocalizedName": city_name,
                "Key": city_key,
                "Date": date}
        Forecast = list()
        for j in range(12):
            a = {"Time": datetime.fromtimestamp(response[j]["EpochDateTime"]).strftime("%H:00"),
                "Temperature": convert_to_C(response[j]["Temperature"]["Value"]),
                "IconPhrase": response[j]["IconPhrase"]}
            Forecast.append(a)
        data["Forecast"] = Forecast
        try:
            with db_connect() as client:
                call = get_collection_data(client, "TwelveHoursForecast")
                call.insert_one(data)
        except:
            print("Cannot connect with DataBase")
        return render_template("TwelveHoursForecast.html", posts=data["Forecast"])
    else:
        return render_template("base.html")
