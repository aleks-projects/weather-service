from flask import Blueprint, request, render_template
import requests
from datetime import datetime
import json
from Support_Functions.Support_Functions import db_connect, convert_to_C, get_collection_data, get_city_key

FiveDay = Blueprint("FiveDay", __name__, template_folder="templates/Forecast_Endpoints", static_folder="static")


@FiveDay.route("/five_day/")
def five_days():
    city_name = request.args.get("city")
    if city_name:
        city_key = get_city_key(city_name)
        date = str(datetime.now().date())
        try:
            with db_connect() as client:
                call = get_collection_data(client, "FiveDayForecast")
                forecast = None
                for i in call.find():
                    if city_key == i["Key"] and date == i["Forecast"][0]["Date"]:
                        forecast = i["Forecast"]
                        break
        except:
            print("Cannot connect with DataBase")
        if forecast is not None:
            return render_template("FiveDayForecast.html", posts=forecast)
        params_2 = {"locationKey": city_key, "apikey": "d3XkGe1PEMS330oLGOw43MJf0J2deNdc"}
        response = requests.get('http://dataservice.accuweather.com/forecasts/v1/daily/5day/{locationKey}',
                                params=params_2)
        response = json.loads(response.text)
        data = {"LocalizedName": city_name,
                "Key": city_key}
        Forecast = list()
        for j in range(5):
            a = {"Date": datetime.utcfromtimestamp(response["DailyForecasts"][j]["EpochDate"]).strftime('%Y-%m-%d'),
                 "Temperature":
                     {"Minimum": convert_to_C(response["DailyForecasts"][j]["Temperature"]["Minimum"]["Value"]),
                      "Maximum": convert_to_C(response["DailyForecasts"][j]["Temperature"]["Maximum"]["Value"])
                      },
                 "IconPhrase": response["DailyForecasts"][j]["Day"]["IconPhrase"]}
            Forecast.append(a)
        data["Forecast"] = Forecast
        try:
            with db_connect() as client:
                call = get_collection_data(client, "FiveDayForecast")
                call.insert_one(data)
        except:
            print("Cannot connect with DataBase")
        return render_template("FiveDayForecast.html", posts=data["Forecast"])
    else:
        return render_template("base.html")
