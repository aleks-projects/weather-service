from flask import Flask
from Forecast_Endpoints.Five_Day_Forecast import FiveDay
from Forecast_Endpoints.Twelve_Hours_Forecast import TwelveHours

app = Flask(__name__)
app.register_blueprint(FiveDay, url_prefix="/api/v1")
app.register_blueprint(TwelveHours, url_prefix="/api/v1")

if __name__ == "__main__":
    app.run('0.0.0.0', port=5432)
