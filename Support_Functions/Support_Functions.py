from pymongo import MongoClient
import json
import requests
from flask import render_template


def convert_to_C(F: float):
    return str(round((F - 32) / 1.8, 1))


def db_connect():
    return MongoClient("172.17.0.2", 27017)


def get_collection_data(client, collection):
    db = client.weather
    call = db[collection]
    return call


def get_city_key(LocalizedName: str):
    try:
        with db_connect() as client:
            call = get_collection_data(client, "city_id")
            city_key = None
            for db_dict in call.find():
                if LocalizedName == db_dict["LocalizedName"]:
                    city_key = db_dict["Key"]
                    break
    except:
        print("Cannot connect with DataBase")
    if city_key is not None:
        return city_key
    params = {"apikey": "d3XkGe1PEMS330oLGOw43MJf0J2deNdc", "q": LocalizedName}
    response = requests.get('http://dataservice.accuweather.com/locations/v1/cities/search', params=params)
    for i in json.loads(response.text):
        if i["Country"]["ID"] == "PL":
            city_key = i["Key"]
            data = {"LocalizedName": LocalizedName, "Key": city_key, "Country": i["Country"]["ID"]}
            try:
                with db_connect() as client:
                    call = get_collection_data(client, "city_id")
                    call.insert_one(data)
            except:
                print("Cannot connect with DataBase")
        break
    if city_key is None:
        return "City not found"
    return city_key




